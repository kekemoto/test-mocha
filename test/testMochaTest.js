"use strict"
var assert = require("assert")
var calc = require("../testMocha.js")

describe("testMocha", function() {
	it("add",function(){
		assert.equal(4, calc.add(1,3))
		assert.equal(5, calc.add(1,3))
	})

	it("multiply", function(){
		assert.equal(6, calc.multiply(2,3))
	})

})

