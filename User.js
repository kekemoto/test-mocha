"use strict"

var User = function(name){
	this.name = name
}

User.prototype.save = function(callback){
	if (this.name === "" || this.name == undefined)
		return callback(new Error('name is undefined'))
	callback(null, this.name)
}

module.exports = User
